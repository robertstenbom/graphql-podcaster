exports.NO_AUDIO =
  "Unsupported format or malformed link to episode audio file.";

exports.NOT_A_FEED = "Provided feed URL could not parsed. Is it a feed?";

exports.NO_TAGS =
  "Could not parse tags from provided URL. Is it an audio file?";
