const DataLoader = require("dataloader");
const feedparser = require("feedparser-promised");

const { getId3Tags } = require("../helpers");

/**
 * Use dataloader to prevent us from making subsequent requests
 * when resolving. This is more of a preperation as we only
 * resolve one feed at a time with no complex relations.
 * 
 * Documentation: https://github.com/facebook/dataloader
 */

const createDataLoaders = () => ({
  // Fetches feeds and returns them as JSON.
  feeds: new DataLoader(urls => {
    const promises = urls.map(feedparser.parse);
    return Promise.all(promises);
  }),
  id3: new DataLoader(urls => {
    const promises = urls.map(getId3Tags);
    return Promise.all(promises);
  })
});

module.exports = exports = createDataLoaders;
