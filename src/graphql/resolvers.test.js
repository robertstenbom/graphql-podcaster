const resolvers = require('../graphql/resolvers');
const errors = require('../error_constants');

const noop = () => {};
const log = {
  info: noop,
  error: noop,
};

const badDataloader = {
  feeds: {
    load: jest.fn().mockImplementation(() => {
      return new Promise((resolve, reject) => {
        return reject();
      });
    }),
  },
};

describe('resolvers', () => {
  const {Query, Episode} = resolvers;
  describe('Query.feed', () => {
    it('should throw NOT A FEED if unable to load', async () => {
      const resp = await Query.feed(
        {},
        {url: 'str'},
        {dataloaders: badDataloader, log},
      );
      expect(typeof resp.episodes).toBe(typeof new Error());
      expect(resp.episodes.message).toEqual(errors.NOT_A_FEED);
    });
  });
});
