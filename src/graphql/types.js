const gql = require('graphql-tag');

// GQL currently strips descriptors:
// https://github.com/apollographql/graphql-tag/pull/99
// but we add them anyway.
const types = gql`
  type Episode {
    # The GUID
    id: ID!
    title: String!
    # Description without HTML tags
    description: String
    # Description with HTML tags
    descriptionWithTags: String
    # Dates are converted to UNIX time
    date: Int
    link: String
    # First item from the enclosures array
    audio: Audio!
    categories: [String]
  }

  type Audio {
    url: String!
    # Mime type
    type: String
    length: Int
  }

  type Feed {
    episodes: [Episode]
  }

  type Id3 {
    title: String
    artist: String
    album: String
    year: String
    genre: String
  }

  type Query {
    feed(url: String!): Feed
    id3(url: String!): Id3
  }
`;

module.exports = exports = types;
