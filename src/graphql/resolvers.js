const moment = require('moment');
const striptags = require('striptags');
const errors = require('../error_constants');
const {getAudioItemFromEnclosures, getId3Tags} = require('../helpers');

const resolvers = {
  Query: {
    // Resolves feed requests
    async feed(_, {url}, {dataloaders, log}) {
      const items = await dataloaders.feeds
        .load(url)
        .catch(err => new Error(errors.NOT_A_FEED));

      if (typeof items === typeof new Error()) {
        log.error('Could not parse provided URL', {url});
      } else {
        log.info(`Found ${items.length} Episodes`);
      }

      return {
        episodes: items,
      };
    },
    // Resolves id3 requests
    async id3(_, {url}, {dataloaders, log}) {
      const tags = await dataloaders.id3
        .load(url)
        .catch(err => new Error(errors.NO_TAGS));

      if (typeof tags === typeof new Error()) {
        log.error('Could not parse id3 tags from provided URL', {url});
      }

      log.debug('Parsed tags', {tags});
      return tags;
    },
  },
  // Resolve individual Episode fields
  Episode: {
    id({guid}) {
      return guid;
    },
    categories({meta, categories}) {
      // Categories seems to be more widely put inside the meta object
      // as opposed to the top-level object, so use these first
      if (meta && meta.categories && meta.categories.length > 0) {
        return meta.categories;
      } else if (categories) {
        return categories;
      }

      return [];
    },
    audio({enclosures}) {
      return getAudioItemFromEnclosures(enclosures);
    },
    // Transform all dates to UNIX time.
    date({date}) {
      try {
        return date ? moment(date).unix() : null;
      } catch (e) {
        // Moment can't parse the date object, return as null
        return null;
      }
    },
    description({description}) {
      return description ? striptags(description) : null;
    },
    descriptionWithTags({description}) {
      return description;
    },
  },
};

module.exports = exports = resolvers;
