const mediatags = require("jsmediatags");
const errors = require("./error_constants");

// Get the audio object from enclosure element from a JSON feed
const getAudioItemFromEnclosures = enclosures => {
  // Only allow mp3 for now. Add desired formats.
  const allowedAudioFormats = ["audio/mpeg"];
  const audio = enclosures.find(
    enclosure => allowedAudioFormats.indexOf(enclosure.type) !== -1
  );

  if (!audio) {
    throw new Error(errors.NO_AUDIO);
  }

  return audio;
};

const getId3Tags = async url => {
  return new Promise((resolve, reject) => {
    mediatags.read(url, {
      onSuccess({ tags }) {
        return resolve(tags);
      },
      onError: reject
    });
  });
};

module.exports = {
  getAudioItemFromEnclosures,
  getId3Tags
};
