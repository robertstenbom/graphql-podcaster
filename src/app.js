const Koa = require('koa');
const graphqlHTTP = require('koa-graphql');
const bunyan = require('bunyan');
const mount = require('koa-mount');

const schema = require('./graphql/schema');
const createDataLoaders = require('./graphql/dataloaders');

const {LOG_LEVEL} = process.env;

const log = bunyan.createLogger({
  name: 'podcast-rss-reader',
  level: LOG_LEVEL || 'info',
});

const app = new Koa();

app.log = log;

// Log request/response
app.use(async (ctx, next) => {
  const {request, response} = ctx;
  const start = Date.now();
  await next();
  const time = Date.now() - start;
  ctx.set('X-Response-Time', time);
  log.info({request, response});
});

app.use(
  mount(
    '/graphql',
    graphqlHTTP({
      schema,
      graphiql: true,
      context: {
        dataloaders: createDataLoaders(),
        log,
      },
    }),
  ),
);

module.exports = exports = app;
