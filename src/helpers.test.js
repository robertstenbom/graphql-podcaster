const errors = require("./error_constants");
const { getAudioItemFromEnclosures } = require("./helpers");

describe("getAudioItemFromEnclosures", () => {
  it("should throw an error when empty", () => {
    expect(() => getAudioItemFromEnclosures([])).toThrow(errors.NO_AUDIO);
  });

  it("should throw error when unsupported format", () => {
    const testData = [
      {
        url: "http://testurl.com",
        type: "audio/wav",
        length: "226905886"
      }
    ];
    expect(() => getAudioItemFromEnclosures([])).toThrow(errors.NO_AUDIO);
  });

  it("should return audio object when found", () => {
    const testData = [
      {
        url: "http://testurl.com",
        type: "audio/mpeg",
        length: "226905886"
      }
    ];
    expect(getAudioItemFromEnclosures(testData)).toEqual(testData[0]);
  });

  // Not testing getId3Tags (only unit testing and we assume jsmediatags has their own)
});
