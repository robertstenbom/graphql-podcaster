const app = require('./app');
const gql = require('graphql-tag');
const request = require('supertest');

describe('app', () => {
  let _app;
  // Listen to an ephemeral port so supertest can make requests.
  beforeAll(() => (_app = app.listen()));

  it('should mount graphql at /graphql', async () => {
    const introspection = `
      {
        __schema {
          types {
            name
          }
        }
      }
    `;
    const res = await request(_app)
      .post('/graphql')
      .send({query: introspection});

    expect(res.statusCode).toBe(200);
  });
});
